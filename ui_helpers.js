function StatusBar(selector) {
    var $html = $("<div class=status-bar></div>");
    $(selector).before($html);
    var type;

    this.clear = function() {
        $html.attr("class", "status-bar");
        $html.html("");
    };

    this.error = function(text) {
        type = "error";
        print(text);
    };

    this.log = function(text) {
        type= "normal";
        print(text);
    };

    this.running = function(text) {
        type = "running";
        print(text);
    };

    this.success = function(text) {
        type = "success";
        print(text);
    };

    this.warn = function(text) {
        type = "warn";
        print(text);
    };

    function print(message) {
        $html.attr("class", "status-bar visible "+type);
        $html.html("<p>"+message+"</p>");
    }
}

function Form(selector) {
    var $btn = $(selector).find(":submit");
    var $status = $("<p class=status-message></p>");
    var $flash = $("<div class=flash></div>");
    var type;
    $btn.after($status);

    this.SUCCESS = "success";
    this.ERROR = "error";

    this.cleanUp = function() {
        $(selector+" .errors.present").removeClass("present");
        $(selector+" .errors li").remove();
        $(selector + " .flash").remove();
        return this;
    };

    this.flash = function(flashType, message) {
        $flash.addClass(flashType);
        $flash.text(message);
        $(selector).prepend($flash);
    };

    this.error = function(message) {
        type = "error";
        this.print(message);
        return this;
    };

    this.lock = function() {
        this.cleanUp();
        $btn.attr("disabled", "disabled");
        return this;
    };

    this.log = function(message) {
        type = "normal";
        this.print(message);
    };

    this.print = function(message) {
        $status.attr("class", "status-message "+type);
        $status.html(message);
        return this;
    };

    this.release = function() {
        $btn.removeAttr("disabled");
        return this;
    };

    this.showErrors = function(errors) {
        this.cleanUp();
        console.log(("flash" in errors));
        if("flash" in errors) {
            this.flash(this.ERROR, errors["flash"][0]);
        }
        for(var k in errors) {
            var fieldErrors = errors[k];
            var $target = $("#"+k+" .errors");
            //var $errorContainer = $("<div class=error-container></div>");
            //$target.after($errorContainer);
            for(var i=0; i<fieldErrors.length; i++) {
                $target.append(" <li>"+fieldErrors[i]+"</li>");
            }
            $target.addClass("present");
        }
        return this;
    };

    this.success = function(message) {
        type = "success";
        this.print(message);
    };
}

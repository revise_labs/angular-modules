var app = angular.module("ksDirectives", []);

//Method to add additional attributes to input elements a la scala's 'key -> "value" method
function appendAttributes(element, selector) {
    var outputElement = element.find(selector);
    var attributes = document.getElementById(element.attr("id")).attributes;
    for(var i=0; i<attributes.length; i++) {
        if(attributes[i].nodeName[0] == '_') {
            var cleanName = attributes[i].nodeName.substr(1);
            outputElement.attr(cleanName, attributes[i].nodeValue);
        }
    }
}

app.run(function($templateCache) {
    $templateCache.put("checkboxGroup.html", $("div#checkbox-group-template").html());
    $templateCache.put("selectBox.html", $("div#select-box-template").html());
    $templateCache.put("checkbox.html", $("div#checkbox-template").html());
    $templateCache.put("input.html", $("div#input-template").html());
    $templateCache.put("textBlock.html", $("div#text-block-template").html());
    $templateCache.put("radio.html", $("div#radio-template").html());
    $templateCache.put("flatRadio.html", $("div#flat-radio-template").html());
});

app.directive("checkbox", function($templateCache) {
    return {
        restrict: "E",
        template: $templateCache.get("checkbox.html"),
        scope: { model: "=", label: "@", id: "@" }
    }
});

app.directive("checkboxGroup", function() {
    return {
        restrict: "E",
        scope: {collection: "=", selected: "=model", label: "@", id: "@" },
        templateUrl: "checkboxGroup.html",
        link: function(scope) {

            scope.idField = new ksObject(scope.collection[0]).id();
            scope.labelField = new ksObject(scope.collection[0]).label();

            scope.toggleSelected = function(item) {
                var index = new ksArray(scope.selected).indexOf(item);
                if(index > -1) {
                    scope.selected.splice(index, 1);
                } else {
                    scope.selected.push(item);
                }
            };

            scope.isSelected = function(item) {
                return new ksArray(scope.selected).indexOf(item) > -1;
            };
        }
    }
});

app.directive("datePicker", function() {
    return {
        restrict: "E",
        templateUrl: "input.html",
        scope: {model: "=", label: "@", id: "@"},
        link: function(scope, elm, attrs) {
            scope.type = "text";
            elm.find("input").datepicker({dateFormat: 'yy-mm-dd'});
            appendAttributes(elm, "input");
            if("required" in attrs) elm.find("input").attr("required", "required");
            scope.isRequired = function() {
                return "required" in attrs ;
            };
            scope.getType = function() {
                return "text";
            };
        }

    }
});

app.directive("dialogBox", function($templateCache) {
    return {
        restrict: "E",
        templateUrl: "dialogBox.html",
        transclude: true,
        scope: {title: "@", open: "=", id: "@"}
    }
});

app.directive("field", function() {
    return {
        restrict: "E",
        templateUrl: "input.html",
        scope: {model: "=", label: "@", type: "@", id: "@"},
        link: function(scope, elm, attrs) {
            appendAttributes(elm, "input");
            if("required" in attrs) elm.find("input").attr("required", "required");
            scope.isRequired = function() {
                return "required" in attrs ;
            };
            scope.getType = function() {
                return scope.type == null ? "text" : scope.type;
            };
        }

    }
});

app.directive("flatRadio", function($templateCache) { //Directive used when the user supplies simple Yes/No or Male/Female options.
    return {
        restrict: "E",
        template: $templateCache.get("flatRadio.html"),
        scope: {model: "=", label: "@", id:"@", options: "@"},
        link: function(scope, elm, attrs) {
            scope._options = [];
            scope.name = elm.attr("model");
            scope._model = {};
            scope.isRequired = function() { return "required" in attrs; };
            var options = scope.options.split(", ");
            for(var i=0; i<options.length; i++) {
                var option = options[i].split(" -> ");
                scope._options.push({id: option[0], description: option[1]});
                if(option[0] == scope.model) { scope._model = {id: option[0], description: option[1]  }; }
            }
            scope.$watch("_model.id", function(newVal) { scope.model = newVal; });
        }
    }
});

app.directive("radio", function($templateCache) { //Directive used when the model is one in an array of objects
    return {
        restrict: "E",
        template: $templateCache.get("radio.html"),
        scope: {model: "=", label: "@", id: "@", options: "=" },
        link: function(scope, elm, attrs) {
            var idField = new ksObject(scope.model).id();
            var labelField = new ksObject(scope.model).label();
            scope._options = [];
            scope._model = {id: scope.model[idField], label: scope.model[labelField]};
            scope.isRequired = function() { return "required" in attrs; };
            for(var i=0; i<scope.options.length; i++) {
                var currentOption = scope.options[i];
                scope._options.push({id: currentOption[idField], label: currentOption[labelField]});
            }
            scope.$watch("_model.id", function(newVal) {
                var tmpModel = {}, index;
                tmpModel[idField] = newVal;
                index = new ksArray(scope.options).indexOf(tmpModel);
                scope.model = scope.options[index];
            });
        }
    }
});

app.directive("selectBox", function() {
    return {
        restrict: "E",
        templateUrl: "selectBox.html",
        scope: {model: "=", options: "=", label: "@", defaultValue: "@default", id: "@", labelField: "@"},
        link: function(scope, elm, attrs) {
            scope.isRequired = function() {
                return "required" in attrs ? "required" : "";
            };

            scope._label =  scope.labelField == undefined ? "description" : scope.labelField; //Used for selecting which field to display as the label in the select box

            if(scope.defaultValue) {
                var option = {};
                option[scope._label] = scope.defaultValue;
                scope.options.unshift(option);
            }

            var index = scope.model != null ? new ksArray(scope.options).indexOf(scope.model) : 0;

            scope._model = scope.options[index];

            scope.$watch("_model", function(newVal) { scope.model = newVal; });
        }
    }
});

app.directive("textBlock", function() {
    return {
        restrict: "E",
        templateUrl: "textBlock.html",
        scope: {id: "@", label: "@", model: "="},
        link: function(scope, elm, attrs) {
            appendAttributes(elm, "textarea");
            scope.isRequired = function() {
                return "required" in attrs ? "required" : "";
            }
        }
    }
});

app.directive("transcluder", function() {
    return {
        restrict: "E",
        template: "<div>This is the template <ng-transclude></ng-transclude></div>",
        transclude: true
    }
});